//Corbin Gomez
#include <stdio.h>
#include "course.h"
#include "vector.h"
#include "student.h"
#include <stdlib.h>
#include <string.h>

void display_info(Course *courses, int *count, Student *students, int *stu_count);

int get_choice();

int main(int argc, char **argv)
{
  Course *courses;
  Student *students;
  int count, stu_count, size = 100, stu_size = 10;

  read_courses(argc, argv, &courses, &count, &size);
  read_students(argc, argv, &students, &stu_count, &stu_size);
  display_info(courses, &count, students, &stu_count);
  deallocate(courses, size);
  return 0;
}//main

void display_info(Course *courses, int *count, Student *students, int *stu_count)
{
  int choice = get_choice();
  while(choice != 0)
  {
    switch (choice)
    {
      case 1: find_CRN(courses, count, students, stu_count); break;
      case 2: find_subject(courses, count); break;
      case 3: add_course(courses, count, students, stu_count); break;//function goes here
      case 4: remove_course(courses, count, students, stu_count); break;//function goes here
      default: break;
    }//switch
    choice = get_choice();
  }//while
}//display_info

int get_choice()
{
  int choice;
  do
  {
    printf("\nRSVP Menu\n");
    printf("0. Done.\n");
    printf("1. Find by CRN.\n");
    printf("2. Find by subject.\n");
    printf("3. Add course.\n");
    printf("4. Remove course.\n");
    printf("Your choice (0 - 4): ");
    scanf("%d", &choice); 
    if(choice < 0 || choice > 4)
      printf("Your choice is outside the acceptable range.  Please try again.\n");
  }while(choice < 0 || choice > 4);
  
  return choice;
}//get_choice
