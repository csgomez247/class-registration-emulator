//Corbin Gomez
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "course.h"
#include "vector.h"
#include "student.h"
void find_CRN(Course *courses, int *count, Student *students, int *stu_count)
{
  int crn, i, did_find = 0, j, k;
  printf("Please enter a CRN: ");
  scanf("%d", &crn);
  for(i = 0; i < *count; i++)
  {   //if it finds the crn
    if(crn == courses[i].crn)
    {
      printf("%s ", courses[i].subject);
      printf("%s\n", courses[i].course);
      did_find++;
      for(j = 0; j < *stu_count; j++)
        for(k = 0; k < students[j].class_count; k++)
            //if current student's current crn equals the found crn
          if(students[j].crns[k] == crn)
            printf("%s %s\n", students[j].last_name, students[j].first_name);
    }//if
  }//for
    //if you did not find the CRN
  if(did_find == 0)
    printf("CRN %d not found.\n", crn);
}//find_CRN

void find_subject(Course *courses, int *count)
{
  char subject[80];
  int i, did_find = 0;
  printf("Please enter a subject: ");
  scanf("%s", subject);
  for(i =  0; i < *count; i++)
  {   //if the scanned subject is the same as the subject in the course array
    if(strcmp(subject, courses[i].subject) == 0)
    {
      did_find++;
      printf("%d ", courses[i].crn);
      printf("%s ", courses[i].subject);
      printf("%s\n", courses[i].course);
      did_find++;
    }//if
  }//for
    //if you did not find a course
  if(did_find == 0)
    printf("No courses were found for %s.\n", subject);
}//find_subject

void add_course(Course *courses, int *count, Student *students, int *stu_count)
{
  char input_sid[80];
  int crn, i, j, k, did_find = 0, did_find_crn = 0, duplicate_crn = 0;
  printf("Add Course\n");
  printf("Please enter the SID of the student: ");
  scanf("%s", input_sid);
  
  for(i = 0; i < *stu_count; i++)
      //if the student's sid is the input sid
    if(strcmp((students)[i].sid, input_sid) == 0)
    {   //if the student already has 5 classes
      if((students)[i].class_count == 5)
        printf("You are already taking 5 courses.\n");
      else//if the student has less than 5 classes
      {
        printf("Please enter the CRN: ");
        scanf("%d", &crn);
        for(j = 0; j < *count; j++)
        {   //if the entered CRN is found within courses
          if(crn == (courses)[j].crn)
          {
            for(k = 0; k < (students)[i].class_count; k++)
            {   //if the student is already enrolled in crn
              if((students)[i].crns[k] == crn)
              {
                printf("You are already taking that course.\n");
                did_find_crn++;
                duplicate_crn++;
              }//if
            }//for 
              //if it is not a duplicate crn
            if(duplicate_crn == 0)
            {
              (students)[i].crns[(students)[i].class_count] = crn;
              ((students)[i].class_count)++;
              did_find_crn++;
            }//if
          }//if
        }//for
          //if there is no crn                    
        if(did_find_crn == 0)
            printf("There is no course with CRN #%d\n", crn);
      }//else
      did_find++;
    }//if
    //if the student was not found.
  if(did_find == 0)
    printf("A student with SID #%s was not found.\n", input_sid);  
}//add_course

void remove_course(Course *courses, int *count, Student *students, int *stu_count)
{
  char input_sid[80];
  int crn, i,j, found_crn = 0, found_sid = 0;
  printf("Remove Course\n");
  printf("Please enter the SID of the student: ");
  scanf("%s", input_sid);
  for(i = 0; i < *stu_count && found_sid == 0; i++)
      //if the entered sid is a student
    if(strcmp((students)[i].sid, input_sid) == 0)
    { found_sid++;
      printf("Current courses: ");
      for(j = 0; j < students[i].class_count; j++)
      {
        printf("%d ", students[i].crns[j]);
      }//for
      printf("\nPlease enter the CRN: ");
      scanf("%d", &crn);
      for(j = 0; j < students[i].class_count; j++)
      {   //if the student's crn matches the entered crn
        if(students[i].crns[j] == crn)
        {
          for(; j < students[i].class_count; j++)
            students[i].crns[j] = students[i].crns[j + 1];
          (students[i].class_count)--;
          found_crn++;
        }//if
      }//for
        //if crn not found
      if(found_crn == 0)
        printf("You are not taking that course.\n");
    }//if
    //if sid not found
  if(found_sid == 0)
    printf("A student with SID #%s was not found.\n", input_sid);
}//remove_course

void read_courses(int argc, char **argv, Course **courses, int *count, int *size)
{
  FILE *fp1;//, *fp2;
  fp1 = fopen(argv[1], "r");
  char line[500];
    //if the file is not readable
  if(fp1 == NULL)
  {
    printf("%s not readable.\n", argv[1]);
    exit(1);
  }//if

  initialize(courses);
  *count = 0;

  while(fgets(line, 500, fp1) != NULL)
  {
      //if the first and fourth characters are digits
    if(isdigit(line[0]) && isdigit(line[4]))
    {
        //if array is full
      if(*count == *size)
        resize(courses, size);
      (*courses)[*count].crn = atoi(strtok(line, "\t"));
      strcpy((*courses)[*count].subject, strtok(NULL, "\t"));
        //if the next character is a ^
      if((*courses)[*count].subject[0] == '^')
        strcpy((*courses)[*count].subject, strtok(NULL, "\t"));

      strcpy((*courses)[*count].course, strtok(NULL, "\t"));
      (*count)++;
    }//if
  }//while
  fclose(fp1);
}//read_courses
