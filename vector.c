//Corbin Gomez
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "vector.h"

void deallocate(Course *courses, int size)
{
  free(courses);
}//deallocate

void initialize(Course **courses)
{
  *courses = (Course*)malloc(sizeof(Course) * 100);
}//initialize

void resize(Course **courses, int *size)
{
  int i;
  Course *temp_courses;
  temp_courses = (Course*)malloc(sizeof(Course) * 2 * *size);
  for(i = 0; i < *size; i++)
    temp_courses[i] = (*courses)[i];
  (*courses) = temp_courses;
  *size = *size * 2;
}//resize

void initialize_students(Student **students)
{
  *students = (Student*)malloc(sizeof(Student) * 10);
}//initialize_students

void resize_students(Student **students, int *size)
{
  int i;
  Student *temp_students;
  temp_students = (Student*)malloc(sizeof(Student) * 2 * *size);
  for(i = 0; i < *size; i++)
    temp_students[i] = (*students)[i];
  (*students) = temp_students;
  *size = *size * 2;
}//resize_students
