//Corbin Gomez
#ifndef VECTOR_H
 #define VECTOR_H
#include "course.h"
void deallocate(Course *courses, int size);

void initialize(Course **courses);

void resize(Course **courses, int *size);

void initialize_students(Student **students);

void resize_students(Student **students, int *size);

#endif
