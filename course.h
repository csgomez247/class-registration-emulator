//Corbin Gomez
#ifndef COURSE_H
  #define COURSE_H
#include "student.h"
typedef struct
{
  int crn;
  char subject[4];
  char course[6];
}Course;

void find_CRN(Course *courses, int *count, Student *students, int *stu_count);

void find_subject(Course *courses, int *count);

void add_course(Course *courses, int *count, Student *students, int *stu_count);

void remove_course(Course *courses, int *count, Student *students, int *stu_count);

void read_courses(int argc, char **argv, Course **courses, int *count, int *size);

void read_students(int argc, char **argv, Student **students, int *stu_count, int *stu_size);

#endif
