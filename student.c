//Corbin Gomez
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "vector.h"
#include "student.h"

void read_students(int argc, char **argv, Student **students, int *stu_count, int *stu_size)
{
  FILE *fp;
  fp = fopen(argv[2], "r");
    //if the file cannot be read  
  if(fp == NULL)
  {
    printf("File %s is not readable\n.", argv[2]);
    exit(2);  
  }//if

  char line[500];
  initialize_students(students);
  *stu_count = 0;
  
  while(fgets(line, 500, fp) != NULL)
  {
    char *ptr;
      //if the array is full
    if(*stu_count == *stu_size)
      resize_students(students, stu_size);
    ptr = strtok(line, ",");
    (*students)[*stu_count].last_name = (char*)malloc(sizeof(char) * (strlen(ptr) + 1));
    strcpy((*students)[*stu_count].last_name, ptr);
    ptr = strtok(NULL, ",");
    (*students)[*stu_count].first_name = (char*)malloc(sizeof(char) * (strlen(ptr) + 1));
    strcpy((*students)[*stu_count].first_name, ptr);
    strcpy((*students)[*stu_count].sid, strtok(NULL, ","));
  
    ptr = strtok(NULL, ",");
    int crn_count = 0;
    while(ptr)
    {
      ((*students)[*stu_count].crns)[crn_count] = atoi(ptr);
      ptr = strtok(NULL, ",");
      crn_count++;
      ((*students)[*stu_count].class_count)++;
    }//while
    (*stu_count)++; 
  }//while
  fclose(fp);
}//read_students
