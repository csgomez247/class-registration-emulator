//Corbin Gomez
#ifndef STUDENT_H
  #define STUDENT_H
typedef struct
{
  char *first_name;
  char *last_name;
  char sid[12];
  int crns[5];
  int class_count;
}Student;

void read_students(int argc, char **argv, Student **students, int *stu_count, int *stu_size);

#endif
