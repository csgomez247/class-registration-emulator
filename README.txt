Corbin Gomez ECS30

Class Registration Emulator
---------------------------

Compiles with 'make'

Run executable with './rsvp.out summersession_I.html students.csv'

students.csv contains valid student names, id's, and the classes they are currently taking.
HTML files contain information about courses.
